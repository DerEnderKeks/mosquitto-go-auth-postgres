# mosquitto-go-auth-postgres

This is a SQL schema with accompanying helper scripts for [Eclipse Mosquitto](https://mosquitto.org/) with the [mosquitto-go-auth](https://github.com/iegomez/mosquitto-go-auth) plugin and PostgreSQL as the backend.

The schema is versioned and can be upgraded by simply [applying the missing scripts](#installation-and-migration).

## Features

This schema features a user and group system

- Users can be flagged as `superuser`
- Users can be assigned to groups
- Users and groups can have ACLs assigned to them
  - A user has the effective ACLs of itself and all of its groups
- ACLs are set by `topic` with boolean columns for `subscribe`, `write` and `read` permissions (resulting `flags` are calculated)
- Users and ACLs can be disabled individually (`enabled` column)

## Usage
To use this schema, first install and configure Mosquitto, the mosquitto-go-auth plugin and PostgreSQL as documented in their respective documentation.

### Installation and migration

To install this schema, you need to apply all scripts in [`sql/schema`](sql/schema) in order.

The current schema version in your database can be queried with the following SQL query:

```sql
select version from schema_version order by version desc limit 1;
```

Upgrade the schema by applying all scripts (**in order!**) that start with a higher version number than returned by that query. 
Don't forget to update the queries in the Mosquitto configuration file if they changed.

### Configuration

To make mosquitto-go-auth use this schema, after [configuring Mosquitto to use the plugin](https://github.com/iegomez/mosquitto-go-auth#configuration), you need to configure the following in your mosquitto.conf. 
Please read the plugin documentation to understand what other options are available, this is just what's specifically required for this project.

```ini
# you may add other backends too
auth_opt_backends postgres

auth_opt_pg_host <host>
auth_opt_pg_port 5432
auth_opt_pg_dbname <db>
auth_opt_pg_user <user>
auth_opt_pg_password <password>
auth_opt_pg_connect_tries 3

# don't forget to update these when you update the schema
auth_opt_pg_userquery select password_hash from "users" where lower(username) = lower($1) and enabled = true limit 1
auth_opt_pg_superquery select count(*) from "users" where lower(username) = lower($1) and superuser = true
auth_opt_pg_aclquery select topic from user_all_acls where lower(username) = lower($1) and (flags & $2) = $2
```

### Helper scripts

There are a few helper scripts in [`sql`](sql) that are intended to be used with [Jetbrains DataGrip](https://www.jetbrains.com/datagrip/) (or the Jetbrains Database plugin), since they contain parameters in the form `${parameter_name}` (you need to [enable parameter substitution in strings](https://www.jetbrains.com/help/datagrip/settings-tools-database-user-parameters.html#:~:text=for%20individual%20patterns.-,Substitute%20inside%20SQL%20strings,-Apply%20parameter%20patterns)). You can use them as normal SQL scripts, as long as you replace the parameters.

These are always meant to be used with the latest schema version.

Feel free to open an issue or even better a merge request if you want to have more helper scripts added.

### Operation

You can manage the users, groups and ACLs using the helper scripts and by simply setting values in the database.

For example, to create an admin user, you run the [`create_user.sql`](sql/create_user.sql) script with the parameters:
`username=admin`, `password_hash=<hash>` and `superuser=true`. The hash is dependent on the hash algorithm you are using and can be generated with the [`pw` tool](https://github.com/iegomez/mosquitto-go-auth/blob/master/pw-gen/pw.go) of the plugin.

To assign an ACL, which allows to read and subscribe the `foo/bar` topic, to the `test` user, run the [`create_user_acl.sql`](sql/create_user_acl.sql) script with the parameters: `username=test`, `topic=foo/bar`, `subscribe=true`, `write=false` and `read=true`. To now also add the write permission, simply edit the value in the database (I strongly suggest using a GUI for manual editing):

```sql
update user_acls set write = true where (select id from users where username = 'test') and topic = 'foo/bar';
```

This works exactly the same for groups, but with the [`create_group_acl.sql`](sql/create_group_acl.sql) script. Do not try to edit the `flags` column directly (you can't), it is calculated automatically.

## License

See [License](License).