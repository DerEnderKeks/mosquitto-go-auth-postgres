select groups.id as group_id, groups.name
from groups
         join user_groups ug on groups.id = ug.group_id
         join users on users.id = ug.user_id
where users.username = '${username}';
