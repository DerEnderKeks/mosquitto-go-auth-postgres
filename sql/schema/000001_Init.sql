create table schema_version
(
    version    int primary key,
    updated_at timestamp not null default current_timestamp
);

create table users
(
    id            bigserial primary key,
    username      text    not null unique,
    password_hash text    not null,
    superuser     boolean not null default false,
    enabled       boolean not null default true,
    description   text
);
create unique index idx_users_username on users (lower(username));

create table user_acls
(
    id        bigserial primary key,
    user_id   bigint  not null references users on delete cascade,
    topic     text    not null,
    enabled   boolean not null default true,
    subscribe boolean          default false,
    write     boolean          default false,
    read      boolean          default false,
    flags     smallint generated always as ((((subscribe)::integer << 2) | ((write)::integer << 1) |
                                             (read)::integer)) stored
);

create table groups
(
    id   bigserial primary key,
    name text not null
);
create unique index idx_groups_name on groups (lower(name));

create table user_groups
(
    id       bigserial primary key,
    user_id  bigint not null references users on delete cascade,
    group_id bigint not null references groups on delete cascade
);
create unique index idx_user_groups_user_id_group_id on user_groups (user_id, group_id);

create table group_acls
(
    id        bigserial primary key,
    group_id  bigint  not null references groups on delete cascade,
    topic     text    not null,
    enabled   boolean not null default true,
    subscribe boolean          default false,
    write     boolean          default false,
    read      boolean          default false,
    flags     smallint generated always as ((((subscribe)::integer << 2) | ((write)::integer << 1) |
                                             (read)::integer)) stored
);

create view user_all_acls as
select u.id      as user_id,
       username,
       'user'    as source_type,
       username  as source,
       topic,
       subscribe as subscribe,
       write     as write,
       read      as read,
       flags
from users u
         join user_acls ua on u.id = ua.user_id
union
select u.id as user_id,
       username,
       'group',
       name,
       topic,
       subscribe,
       write,
       read,
       flags
from groups g
         join group_acls ga on g.id = ga.group_id
         join user_groups ug on g.id = ug.group_id
         join users u on u.id = ug.user_id;

insert into schema_version (version)
values (1);